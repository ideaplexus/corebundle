<?php

namespace IPC\CoreBundle\Traits;

trait MergeOptionsTrait
{

    /**
     * Merge default options with input options. Additional keys will be filtered
     *
     * @param array $default default options
     * @param array $input input options
     * @param boolean $recursive if true a recursive walk trough keys of $default and $input if both are arrays
     *
     * @return array filtered and merged options
     */
    protected function mergeOptions($default, $input, $recursive = true)
    {
        $intersect = array_intersect_key($input, $default);
        if ($recursive) {
            foreach (array_keys($intersect) as $key) {
                if (is_array($default[$key]) && is_array($input[$key])) {
                    $input[$key] = $this->mergeOptions($default[$key], $input[$key], $recursive);
                    $intersect   = array_intersect_key($input, $default);
                } // no else
            }
        } // no else
        $diff = array_diff_key($default, $input);

        return $diff + $intersect;
    }

}