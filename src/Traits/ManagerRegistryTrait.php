<?php

namespace IPC\CoreBundle\Traits;

use Doctrine\Common\Persistence\ManagerRegistry;

trait ManagerRegistryTrait
{

    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;

    public function setManagerRegistry(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @return ManagerRegistry
     */
    protected function getManagerRegistry()
    {
        return $this->managerRegistry;
    }
}
