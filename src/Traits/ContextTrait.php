<?php

namespace IPC\CoreBundle\Traits;

trait ContextTrait
{
    /**
     * @var mixed
     */
    protected $context;

    /**
     * @param $context
     *
     * @return $this
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @return bool
     */
    public function hasContext()
    {
        return null !== $this->context;
    }
}