<?php

namespace IPC\CoreBundle\Codec;

use IPC\CoreBundle\Interfaces\CodecInterface;
use IPC\CoreBundle\Traits\MergeOptionsTrait;

class SerializeCodec implements CodecInterface
{

    use MergeOptionsTrait;

    const DEFAULT_OPTIONS = [
        'decode' => [
            'options' => [
                'allowed_classes' => false,
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $options;

    /**
     * JsonCodec constructor.
     *
     * @param array $options
     */
    public function __construct($options = self::DEFAULT_OPTIONS)
    {
        $this->options = $this->mergeOptions(self::DEFAULT_OPTIONS, $options);
    }

    /**
     * @param mixed $data
     *
     * @return mixed
     */
    public function encode($data)
    {
        return serialize($data);
    }

    /**
     * @param mixed $data
     *
     * @return mixed
     */
    public function decode($data)
    {
        $options = $this->options['decode'];

        return unserialize($data, $options['options']);
    }
}