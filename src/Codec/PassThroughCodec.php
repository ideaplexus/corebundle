<?php

namespace IPC\CoreBundle\Codec;

use IPC\CoreBundle\Interfaces\CodecInterface;

class PassThroughCodec implements CodecInterface
{
    /**
     * @param mixed $data
     *
     * @return mixed
     */
    public function encode($data)
    {
        return $data;
    }

    /**
     * @param mixed $data
     *
     * @return mixed
     */
    public function decode($data)
    {
        return $data;
    }
}