<?php

namespace IPC\CoreBundle\Codec;

use IPC\CoreBundle\Interfaces\CodecInterface;
use IPC\CoreBundle\Interfaces\ContextInterface;
use IPC\CoreBundle\Traits\ContextTrait;

class AutoCodec implements CodecInterface, ContextInterface
{

    use ContextTrait;

    /**
     * @var CodecInterface
     */
    protected $codec;

    public function __construct(CodecInterface $codec)
    {
        $this->codec = $codec;
    }

    /**
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \RuntimeException
     */
    public function encode($data)
    {
        if (!$this->hasContext()) {
            throw new \RuntimeException('Context is required.');
        }

        if ($this->getContext() === true) {
            return $this->codec->encode($data);
        } // no else

        return $data;
    }

    /**
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \RuntimeException
     */
    public function decode($data)
    {
        if (!$this->hasContext()) {
            throw new \RuntimeException('Context is required.');
        }

        if ($this->getContext() === true) {
            return $this->codec->decode($data);
        } // no else

        return $data;
    }
}