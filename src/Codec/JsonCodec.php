<?php

namespace IPC\CoreBundle\Codec;

use IPC\CoreBundle\Interfaces\CodecInterface;
use IPC\CoreBundle\Traits\MergeOptionsTrait;

class JsonCodec implements CodecInterface
{

    use MergeOptionsTrait;

    const DEFAULT_OPTIONS = [
        'encode' => [
            'options' => 0,
            'depth'   => 512,
        ],
        'decode' => [
            'assoc' => true,
            'options' => 0,
            'depth'   => 512,
        ],
    ];

    /**
     * @var array
     */
    protected $options;

    /**
     * JsonCodec constructor.
     *
     * @param array $options
     */
    public function __construct($options = self::DEFAULT_OPTIONS)
    {
        $this->options = $this->mergeOptions(self::DEFAULT_OPTIONS, $options);
    }

    /**
     * @param mixed $data
     *
     * @return mixed
     */
    public function encode($data)
    {
        $options = $this->options['encode'];

        return json_encode($data, $options['options'], $options['depth']);
    }

    /**
     * @param mixed $data
     *
     * @return mixed
     */
    public function decode($data)
    {
        $options = $this->options['decode'];

        return json_decode($data, $options['assoc'], $options['depth'], $options['options']);
    }
}