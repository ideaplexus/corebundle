<?php

namespace IPC\CoreBundle\Interfaces;

interface ContextInterface
{
    /**
     * @param mixed $context
     *
     * @return mixed
     */
    public function setContext($context);
}