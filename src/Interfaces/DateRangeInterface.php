<?php

namespace IPC\CoreBundle\Interfaces;

interface DateRangeInterface
{

    /**
     * Get start date of date range
     *
     * @return \DateTime
     */
    public function getStartDate();

    /**
     * Get end date of date range
     *
     * @return \DateTime
     */
    public function getEndDate();
}
