<?php

namespace IPC\CoreBundle\Interfaces;

interface CodecInterface
{
    /**
     * @param mixed $data
     *
     * @return mixed
     */
    public function encode($data);

    /**
     * @param mixed $data
     *
     * @return mixed
     */
    public function decode($data);
}