<?php

namespace IPC\CoreBundle\Form\Transformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query;
use IPC\CoreBundle\Interfaces\CodecInterface;
use IPC\CoreBundle\Interfaces\ContextInterface;
use IPC\CoreBundle\Traits\ManagerRegistryTrait;
use IPC\CoreBundle\Traits\MergeOptionsTrait;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class EntityToIdTransformer implements DataTransformerInterface
{

    use ManagerRegistryTrait;
    use MergeOptionsTrait;

    const DEFAULT_OPTIONS = [
        'multiple'         => false,
        'partialReference' => true,
    ];

    /**
     * @var array
     */
    protected $options;

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var CodecInterface
     */
    protected $transformerCodec;

    /**
     * @var array
     */
    protected $identifierFieldNames;



    /**
     * @param string        $entityClass
     * @param array         $options
     */
    public function __construct($entityClass, CodecInterface $transformerCodec, array $options = [])
    {
        $this->entityClass      = $entityClass;
        $this->transformerCodec = $transformerCodec;
        $this->options          = $this->mergeOptions(self::DEFAULT_OPTIONS, $options);
    }

    /**
     * Transforms a value from the original representation to a transformed representation.
     *
     * This method is called on two occasions inside a form field:
     *
     * 1. When the form field is initialized with the data attached from the datasource (object or array).
     * 2. When data from a request is submitted using {@link Form::submit()} to transform the new input data
     *    back into the renderable format. For example if you have a date field and submit '2009-10-10'
     *    you might accept this value because its easily parsed, but the transformer still writes back
     *    "2009/10/10" onto the form field (for further displaying or other purposes).
     *
     * This method must be able to deal with empty values. Usually this will
     * be NULL, but depending on your implementation other empty values are
     * possible as well (such as empty strings). The reasoning behind this is
     * that value transformers must be chainable. If the transform() method
     * of the first value transformer outputs NULL, the second value transformer
     * must be able to process that value.
     *
     * By convention, transform() should return an empty string if NULL is
     * passed.
     *
     * @param mixed $value The value in the original representation
     *
     * @return mixed The value in the transformed representation
     *
     * @throws TransformationFailedException When the transformation fails.
     */
    public function transform($value)
    {
        if ($value === null) {
            return '';
        }

        if (!$this->options['multiple']) {
            $value = [$value];
        }

        /* @var array $value */
        foreach ($value as $entity) {
            if (!is_a($entity, $this->entityClass)) {
                throw new TransformationFailedException('Given value does not match expected class.');
            }
        }

        $identifiers = [];
        $propertyAccessor = new PropertyAccessor();
        foreach ($value as $entity) {
            $itemIdentifiers = [];
            foreach ($this->getIdentifierFieldNames() as $identifierFieldName) {
                try {
                    $itemIdentifiers[$identifierFieldName] = $propertyAccessor->getValue($entity, $identifierFieldName);
                } catch (\Exception $e) {
                    throw new TransformationFailedException('Failed to transform entity.', $e->getCode(), $e);
                }
            }
            $identifiers[] = count($itemIdentifiers) > 1 ? $itemIdentifiers : reset($itemIdentifiers);
        }

        return $this->options['multiple']
            ? $this->encode($identifiers)
            : $this->encode(reset($identifiers));
    }

    /**
     * Transforms a value from the transformed representation to its original
     * representation.
     *
     * This method is called when {@link Form::submit()} is called to transform the requests tainted data
     * into an acceptable format for your data processing/model layer.
     *
     * This method must be able to deal with empty values. Usually this will
     * be an empty string, but depending on your implementation other empty
     * values are possible as well (such as empty strings). The reasoning behind
     * this is that value transformers must be chainable. If the
     * reverseTransform() method of the first value transformer outputs an
     * empty string, the second value transformer must be able to process that
     * value.
     *
     * By convention, reverseTransform() should return NULL if an empty string
     * is passed.
     *
     * @param mixed $value The value in the transformed representation
     *
     * @return mixed The value in the original representation
     *
     * @throws TransformationFailedException When the transformation fails.
     */
    public function reverseTransform($value)
    {
        if ('' === $value || null === $value) {
            if ($this->options['multiple']) {
                return [];
            } // no else
            return null;
        }

        $value = $this->decode($value);
        if (!$value) {
            throw new TransformationFailedException('Value could not be transformed into object(s)');
        }

        $normalizedIds = $this->getNormalizedIdentifiers($value);
        $entities = $this->queryEntities($normalizedIds);
        $missingIds = $this->getMissingIds($normalizedIds, $entities);

        if (count($missingIds) > 0) {
            throw new TransformationFailedException('Id(s) could not be transformed into object(s)');
        }

        return $this->options['multiple'] ? $entities : reset($entities);
    }

    /**
     * @param $ids
     * @param $entities
     *
     * @return array
     */
    protected function getMissingIds($ids, $entities)
    {
        $missingIds = [];
        $collection = new ArrayCollection($entities);

        /* @var $keys array */
        foreach ($ids as $keys) {
            $criteria = Criteria::create();
            $criteria->setMaxResults(1);
            foreach ($keys as $key => $value) {
                if (($type = $this->getTypeOfField($key)) === null) {
                    if (is_numeric($value)) {
                        $value += 0;
                    }
                } else {
                    settype($value, $type);
                }
                $criteria->andWhere(Criteria::expr()->eq($key, $value));
            }
            if ($collection->matching($criteria)->count() === 0) {
                $missingIds[] = $keys;
            }
        }

        return $missingIds;
    }

    /**
     * @param array $normalizedIds
     *
     * @return array
     */
    protected function queryEntities($normalizedIds)
    {
        /* @var $manager \Doctrine\ORM\EntityManager */
        $manager = $this
            ->getManagerRegistry()
            ->getManagerForClass($this->entityClass);

        $builder = $manager
            ->getRepository($this->entityClass)
            ->createQueryBuilder('ec');

        $paramCounter = 0;
        $orX  = $builder->expr()->orX();

        foreach ($normalizedIds as $ids) {
            $andX = $builder->expr()->andX();
            foreach ($ids as $name => $value) {
                $paramIdentifier = $name . $paramCounter++;
                $andX->add("ec.$name = :$paramIdentifier");
                $builder->setParameter($paramIdentifier, $value);
            }
            $orX->add($andX);
        }
        $builder->where($orX);

        // overwrite default select
        if (true === $this->options['partialReference']) {
            $select = [];
            foreach ($this->getIdentifierFieldNames() as $fieldName) {
                $select[] = "ec.$fieldName";
            }
            $builder->select(implode(', ', $select));
        }

        $query = $builder->getQuery();

        if (true === $this->options['partialReference']) {
            $query->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);
            $results = [];
            foreach ((array)$query->getResult() as $result) {
                $results[] = $manager->getPartialReference($this->entityClass, $result);
            }
            return $results;
        }

        return $query->getResult();
    }

    /**
     * @param array|mixed $identifiers

     * @return array
     *
     * @throws TransformationFailedException
     */
    protected function getNormalizedIdentifiers($identifiers)
    {
        // assume that an single entity is requested
        if (!is_array($identifiers)) {
            // but not enough data where transferred
            if (count($this->getIdentifierFieldNames()) > 1) {
                throw new TransformationFailedException('Invalid input, missing identifier(s)');
            } // no else
            $identifiers = [$identifiers];
        }

        // if input is an array and multiple entries are allowed
        if ($this->options['multiple']) {
            $identifierFieldNameCount = count($this->getIdentifierFieldNames());
            $normalized = [];
            if ($identifierFieldNameCount > 1) {
                foreach ($identifiers as $identifier) {
                    if (!is_array($identifier) || count($identifier) !== $identifierFieldNameCount) {
                        throw new TransformationFailedException('Invalid input, missing identifier(s)');
                    } // no else

                    // check if identifier values already associated
                    if (!$this->isArrayAllKeyInt($identifier)) {
                        // wrong associations
                        if (!empty(array_diff($this->getIdentifierFieldNames(), array_keys($identifier)))) {
                            throw new TransformationFailedException('Invalid input, missing identifier(s)');
                        }
                        $normalized[] = $identifier;
                    } else {
                        $normalized[] = array_combine($this->getIdentifierFieldNames(), $identifier);
                    }
                }
            } else {
                $fieldNames = $this->getIdentifierFieldNames();
                $fieldName = reset($fieldNames);
                foreach ($identifiers as $identifier) {
                    $normalized[] = [$fieldName => $identifier];
                }
            }
            return $normalized;
        }

        // if not, requested entity could have multiple identifiers

        // count matches
        if (count($identifiers) !== count($this->getIdentifierFieldNames())) {
            throw new TransformationFailedException('Invalid input, missing identifier(s)');
        } // no else

        // check if identifier values already associated
        if (!$this->isArrayAllKeyInt($identifiers)) {
            // wrong associations
            if (!empty(array_diff($this->getIdentifierFieldNames(), array_keys($identifiers)))) {
                throw new TransformationFailedException('Invalid input, missing identifier(s)');
            }
            return [$identifiers];
        }

        return [array_combine($this->getIdentifierFieldNames(), $identifiers)];
    }

    /**
     * Encode given data with given codec
     *
     * @param mixed $data
     *
     * @return string
     */
    protected function encode($data)
    {
        if ($this->transformerCodec instanceof ContextInterface) {
            $context = count($this->getIdentifierFieldNames()) > 1 || $this->options['multiple'];
            $this->transformerCodec->setContext($context);
        }

        return $this->transformerCodec->encode($data);
    }

    /**
     * Decode given string with given codec
     *
     * @param string $data
     *
     * @return mixed
     */
    protected function decode($data)
    {
        if ($this->transformerCodec instanceof ContextInterface) {
            $context = (count($this->getIdentifierFieldNames()) > 1 || $this->options['multiple']) && is_string($data);
            $this->transformerCodec->setContext($context);
        }

        return $this->transformerCodec->decode($data);
    }

    /**
     * Check an array if all keys are int
     *
     * @param array $input
     *
     * @return bool
     */
    protected function isArrayAllKeyInt($input)
    {
        if (!is_array($input)) return false;
        if (count($input) <= 0) return true;

        return array_unique(array_map('is_int', array_keys($input))) === [true];
    }

    /**
     * Get the identifier field names for the given entity
     *
     * @return array
     */
    protected function getIdentifierFieldNames()
    {
        if (!$this->identifierFieldNames) {
            $this->identifierFieldNames = $this
                ->getManagerRegistry()
                ->getManagerForClass($this->entityClass)
                ->getClassMetadata($this->entityClass)
                ->getIdentifierFieldNames();
        }

        return $this->identifierFieldNames;
    }

    /**
     * Get the type of a field name
     *
     * @param $fieldName
     *
     * @return string|null
     */
    protected function getTypeOfField($fieldName)
    {
        return $this
            ->getManagerRegistry()
            ->getManagerForClass($this->entityClass)
            ->getClassMetadata($this->entityClass)
            ->getTypeOfField($fieldName)
            ;
    }
}
