# IPC - CoreBundle

## Usage

### Codec

The `CodecInterface` as allows you to encode/decode data. With this bundle there are different codecs included.

### EntityToIdTransformer

The `EntityToIdTransformer` allows you to transform entities it ids and vice versa in Symfony forms.

## Development

### Use Docker

Run a docker container

```bash
docker run --name corebundle -v $PWD:/var/www -d ideaplexus/php:7.0-fpm
```

Enter the container

```bash
docker exec -it corebundle bash
```

### Installation of dependencies

```bash
composer install
```

### Test

```bash
vendor/bin/phpunit
```

