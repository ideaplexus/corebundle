<?php

namespace IPC\Tests\CoreBundle\Codec;

use IPC\CoreBundle\Codec\SerializeCodec;
use PHPUnit\Framework\TestCase;

class SerializeCodecTest extends TestCase
{

    public function providerEncodeDecode()
    {
        return [
            [ null ],
            [ '' ],
            [ [] ],
            [ 'some value' ],
            [ 123 ],
            [ 12.34 ],
        ];
    }

    /**
     * @param mixed $data
     *
     * @dataProvider providerEncodeDecode
     */
    public function testEncodeDecode($data)
    {
        $codec = new SerializeCodec();
        $encoded = $codec->encode($data);
        $decoded = $codec->decode($encoded);

        $this->assertEquals($data, $decoded);
    }

    public function testObjectEncodeDecode()
    {
        $sampleClass = new SampleClass();
        $otherSampleClass = new OtherSampleClass();

        $codec = new SerializeCodec(['decode' => ['options' => ['allowed_classes' => [OtherSampleClass::class]]]]);
        $encoded = $codec->encode($sampleClass);
        $decoded = $codec->decode($encoded);
        $this->assertInstanceOf(\__PHP_Incomplete_Class::class, $decoded);

        $encoded = $codec->encode($otherSampleClass);
        $decoded = $codec->decode($encoded);

        $this->assertEquals($otherSampleClass, $decoded);
    }
}

class SampleClass
{
}

class OtherSampleClass
{
}