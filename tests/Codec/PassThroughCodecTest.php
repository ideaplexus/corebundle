<?php

namespace IPC\Tests\CoreBundle\Codec;

use IPC\CoreBundle\Codec\PassThroughCodec;
use PHPUnit\Framework\TestCase;

class PassThroughCodecTest extends TestCase
{
    /**
     * @var PassThroughCodec
     */
    protected $codec;

    public function setUp()
    {
        $this->codec = new PassThroughCodec();
    }


    public function testEncodeDecode()
    {
        $data = 'any data';

        $this->assertEquals($data, $this->codec->encode($data));
        $this->assertEquals($data, $this->codec->decode($data));
    }
}