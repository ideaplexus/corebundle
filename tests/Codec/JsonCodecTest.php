<?php

namespace IPC\Tests\CoreBundle\Codec;

use IPC\CoreBundle\Codec\JsonCodec;
use PHPUnit\Framework\TestCase;

class JsonCodecTest extends TestCase
{
    /**
     * @var JsonCodec
     */
    protected $codec;

    public function setUp()
    {
        $this->codec = new JsonCodec();
    }

    public function providerEncodeDecode()
    {
        return [
            [ null ],
            [ '' ],
            [ [] ],
            [ 'some value' ],
            [ 123 ],
            [ 12.34 ],
        ];
    }

    /**
     * @param mixed $data
     *
     * @dataProvider providerEncodeDecode
     */
    public function testEncodeDecode($data)
    {
        $encoded = $this->codec->encode($data);
        $decoded = $this->codec->decode($encoded);

        $this->assertEquals($data, $decoded);
    }
}