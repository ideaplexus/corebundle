<?php

namespace IPC\Tests\CoreBundle\Codec;

use IPC\CoreBundle\Codec\AutoCodec;
use IPC\CoreBundle\Interfaces\CodecInterface;
use PHPUnit\Framework\TestCase;

class AutoCodecTest extends TestCase
{

    public function testEncodeException()
    {
        $codec = new AutoCodec($this->createMock(CodecInterface::class));
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Context is required.');
        $codec->encode('test');
    }

    public function testDecodeException()
    {
        $codec = new AutoCodec($this->createMock(CodecInterface::class));
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Context is required.');
        $codec->decode('test');
    }

    public function encodeDecode()
    {
        $wrappedCodec = $this->createMock(CodecInterface::class);
        $wrappedCodec
            ->expects($this->once())
            ->method('encode')
            ->willReturnCallback(function($data) {
                return str_rot13($data);
            });
        $wrappedCodec
            ->expects($this->once())
            ->method('decode')
            ->willReturnCallback(function($data) {
                return str_rot13($data);
            });

        $codec = new AutoCodec($wrappedCodec);

        $data = 'test';

        $codec->setContext(false);
        $codec->encode($data);
        $encoded = $codec->encode($data);
        $decoded = $codec->decode($encoded);

        $this->assertEquals($data, $encoded);
        $this->assertEquals($data, $decoded);

        $codec->setContext(true);
        $encoded = $codec->encode($data);
        $decoded = $codec->decode($encoded);

        $this->assertNotEquals($data, $encoded);
        $this->assertEquals($data, $decoded);
    }
}