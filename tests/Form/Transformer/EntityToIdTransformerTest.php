<?php

namespace IPC\Tests\CoreBundle\Form\Transformer;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use IPC\CoreBundle\Codec\AutoCodec;
use IPC\CoreBundle\Codec\JsonCodec;
use IPC\CoreBundle\Form\Transformer\EntityToIdTransformer;
use PHPUnit\Framework\TestCase;

class EntityToIdTransformerTest extends TestCase
{

    /**
     * @var Registry|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $registry;

    /**
     * @var EntityManager|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $entityManager;

    /**
     * @var ClassMetadata|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $classMetadata;

    /**
     * @var EntityRepository|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $entityRepository;

    /**
     * @var QueryBuilder|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $queryBuilder;

    /**
     * @var AbstractQuery|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $query;

    protected function setUp()
    {
        parent::setUp();

        $this->registry         = $this->createMock(Registry::class);
        $this->entityManager    = $this->createMock(EntityManager::class);
        $this->classMetadata    = $this->createMock(ClassMetadata::class);
        $this->entityRepository = $this->createMock(EntityRepository::class);
        $this->queryBuilder     = $this->createMock(QueryBuilder::class);
        $this->query            = $this->createMock(AbstractQuery::class);
    }

    public function providerGetNormalizedIdentifiers()
    {
        return [
            [
                [ 'multiple' => false ], // options
                ['idOne'],               // identifierFieldNames
                1,                       // input
                [['idOne' => 1]],        // output
            ],
            [
                [ 'multiple' => false ],
                ['idOne'],
                [1],
                [['idOne' => 1]],
            ],
            [
                [ 'multiple' => false ],
                ['idOne'],
                ['idOne' => 1],
                [['idOne' => 1]],
            ],
            [
                [ 'multiple' => false ],
                ['idOne', 'idTwo'],
                [1, 'two'],
                [['idOne' => 1, 'idTwo' => 'two']],
            ],
            [
                [ 'multiple' => false ],
                ['idOne', 'idTwo'],
                ['idOne' => 1, 'idTwo' => 'two'],
                [['idOne' => 1, 'idTwo' => 'two']]
            ],
            [
                [ 'multiple' => true ],
                ['idOne'],
                [1],
                [['idOne' => 1]],
            ],
            [
                [ 'multiple' => true ],
                ['idOne'],
                [1, 2],
                [['idOne' => 1], ['idOne' => 2]],
            ],
            [
                [ 'multiple' => true ],
                ['idOne', 'idTwo'],
                [[1, 'two'], [2, 'three']],
                [['idOne' => 1, 'idTwo' => 'two'], ['idOne' => 2, 'idTwo' => 'three']],
            ],
            [
                [ 'multiple' => true ],
                ['idOne', 'idTwo'],
                [[1, 'two'], [2, 'three']],
                [['idOne' => 1, 'idTwo' => 'two'], ['idOne' => 2, 'idTwo' => 'three']],
            ],
            [
                [ 'multiple' => true ],
                ['idOne', 'idTwo'],
                [['idOne' => 1, 'idTwo' => 'two'], [2, 'three']],
                [['idOne' => 1, 'idTwo' => 'two'], ['idOne' => 2, 'idTwo' => 'three']],
            ],
        ];
    }

    /**
     * @dataProvider providerGetNormalizedIdentifiers
     *
     * @param $options
     * @param $identifierFieldNames
     * @param $input
     * @param $output
     */
    public function testGetNormalizedIdentifiers($options, $identifierFieldNames, $input, $output)
    {
        $this->registry
            ->expects($this->exactly(1))
            ->method('getManagerForClass')
            ->willReturn($this->entityManager);

        $this->entityManager
            ->expects($this->exactly(1))
            ->method('getClassMetadata')
            ->willReturn($this->classMetadata);

        $this->classMetadata
            ->expects($this->exactly(1))
            ->method('getIdentifierFieldNames')
            ->willReturn($identifierFieldNames);

        $transformer = new EntityToIdTransformer(Entity::class, new AutoCodec(new JsonCodec()), $options);
        $transformer->setManagerRegistry($this->registry);

        $reflectionMethod = new \ReflectionMethod(EntityToIdTransformer::class, 'getNormalizedIdentifiers');
        $reflectionMethod->setAccessible(true);

        $this->assertEquals($output, $reflectionMethod->invoke($transformer, $input));
    }

    public function providerGetMissingIds()
    {
        return [
            [
                'ids' => [
                    [
                        'idOne' => 1
                    ]
                ],
                'entities' => [
                    new Entity(1),
                ],
                'output' => []
            ],
            [
                'ids' => [
                    [
                        'idOne' => '1'
                    ]
                ],
                'entities' => [
                    new Entity(1),
                ],
                'output' => []
            ],
            [
                'ids' => [
                    [
                        'idOne' => 1
                    ]
                ],
                'entities' => [
                    new Entity(1),
                ],
                'output' => []
            ],
            [
                'ids' => [
                    [
                        'idOne' => 1,
                        'idTwo' => 'two'
                    ],
                    [
                        'idOne' => 2,
                        'idTwo' => 'three'
                    ],
                ],
                'entities' => [
                    new Entity(1, 'two'),
                    new Entity(2, 'three'),
                ],
                'output' => [],
            ],
            [
                'ids' => [
                    [
                        'idOne' => 1,
                        'idTwo' => 'two'
                    ],
                    [
                        'idOne' => 2,
                        'idTwo' => 'three'
                    ],
                ],
                'entities' => [
                    new Entity(1, 'two'),
                ],
                'output' => [
                    [
                        'idOne' => 2,
                        'idTwo' => 'three'
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider providerGetMissingIds
     *
     * @param $ids
     * @param $entities
     * @param $output
     */
    public function testGetMissingIds($ids, $entities, $output)
    {
        $count = 0;
        $returnValueMap = [];
        foreach ($ids as $entity) {
            foreach ($entity as $property => $value) {
                $count++;
                $returnValueMap[] = [gettype($value), $property];
            }
        }

        $this->registry
            ->expects($this->exactly($count))
            ->method('getManagerForClass')
            ->willReturn($this->entityManager);

        $this->entityManager
            ->expects($this->exactly($count))
            ->method('getClassMetadata')
            ->willReturn($this->classMetadata);

        $this->classMetadata
            ->expects($this->exactly($count))
            ->method('getTypeOfField')
            ->willReturnMap($returnValueMap);

        $transformer = new EntityToIdTransformer(Entity::class, new AutoCodec(new JsonCodec()));
        $transformer->setManagerRegistry($this->registry);

        $reflectionMethod = new \ReflectionMethod(EntityToIdTransformer::class, 'getMissingIds');
        $reflectionMethod->setAccessible(true);

        $this->assertEquals($output, $reflectionMethod->invoke($transformer, $ids, $entities));
    }

    public function providerTransform()
    {
        $entityOne   = new Entity(1);
        $entityTwo   = new Entity(2, 'two');
        $entityThree = new Entity(3, 'three');

        return [
            [
                [ 'multiple' => false ], // options
                ['idOne'],               // identifierFieldNames
                $entityOne,              // input
                1,                       // output
            ],
            [
                [ 'multiple' => false ],
                ['idOne', 'idTwo'],
                $entityTwo,
                '{"idOne":2,"idTwo":"two"}',
            ],
            [
                [ 'multiple' => true ],
                ['idOne'],
                [$entityOne],
                '[1]',
            ],
            [
                [ 'multiple' => true ],
                ['idOne'],
                [$entityOne, $entityTwo],
                '[1,2]',
            ],
            [
                [ 'multiple' => true ],
                ['idOne', 'idTwo'],
                [$entityTwo],
                '[{"idOne":2,"idTwo":"two"}]',
            ],
            [
                [ 'multiple' => true ],
                ['idOne', 'idTwo'],
                [$entityTwo, $entityThree],
                '[{"idOne":2,"idTwo":"two"},{"idOne":3,"idTwo":"three"}]',
            ],
        ];
    }

    /**
     * @dataProvider providerTransform
     *
     * @param $options
     * @param $identifierFieldNames
     * @param $input
     * @param $output
     */
    public function testTransform($options, $identifierFieldNames, $input, $output)
    {
        $this->registry
            ->expects($this->exactly(1))
            ->method('getManagerForClass')
            ->willReturn($this->entityManager);

        $this->entityManager
            ->expects($this->exactly(1))
            ->method('getClassMetadata')
            ->willReturn($this->classMetadata);

        $this->classMetadata
            ->expects($this->exactly(1))
            ->method('getIdentifierFieldNames')
            ->willReturn($identifierFieldNames);

        $transformer = new EntityToIdTransformer(Entity::class, new AutoCodec(new JsonCodec()), $options);
        $transformer->setManagerRegistry($this->registry);

        $this->assertEquals($output, $transformer->transform($input));
    }

    public function providerReverseTransform()
    {
        $entityOne = new Entity(1);
        $entityTwo = new Entity(2, 'two');

        return [
            [[
                'options'              => [ 'multiple' => false, 'partialReference' => false ],
                'identifierFieldNames' => ['idOne'],
                'input'                => '',
                'output'               => 0,
                'expects'              => [
                    'getManagerForClass'      => 0,
                    'getClassMetadata'        => 0,
                    'getRepository'           => 0,
                    'getIdentifierFieldNames' => 0,
                    'getTypeOfField'          => 0,
                    'createQueryBuilder'      => 0,
                    'getQuery'                => 0,
                    'getResult'               => 0,
                    'expr'                    => 0,
                    'setParameter'            => 0,
                    'where'                   => 0,
                    'select'                  => 0,
                ],
                'result'               => [],
                'partialReferences'    => [],
            ]],
            [[
                'options'              => [ 'multiple' => false, 'partialReference' => true ],
                'identifierFieldNames' => ['idOne'],
                'typeOfField'          => [['idOne', 'integer']],
                'input'                => '1',
                'output'               => $entityOne,
                'expects'              => [
                    'getManagerForClass'      => 3,
                    'getClassMetadata'        => 2,
                    'getRepository'           => 1,
                    'getIdentifierFieldNames' => 1,
                    'getTypeOfField'          => 1,
                    'createQueryBuilder'      => 1,
                    'getQuery'                => 1,
                    'getResult'               => 1,
                    'expr'                    => 2,
                    'setParameter'            => 1,
                    'where'                   => 1,
                    'select'                  => 1,
                ],
                'result'               => [1],
                'partialReferences'    => [$entityOne],
            ]],
            [[
                'options'              => [ 'multiple' => false, 'partialReference' => true ],
                'identifierFieldNames' => ['idOne', 'idTwo'],
                'typeOfField'          => [['idOne', 'integer'], ['idTwo', 'string']],
                'input'                => json_encode([2, 'two']),
                'output'               => $entityTwo,
                'expects'              => [
                    'getManagerForClass'      => 4,
                    'getClassMetadata'        => 3,
                    'getRepository'           => 1,
                    'getIdentifierFieldNames' => 1,
                    'getTypeOfField'          => 2,
                    'createQueryBuilder'      => 1,
                    'getQuery'                => 1,
                    'getResult'               => 1,
                    'expr'                    => 2,
                    'setParameter'            => 2,
                    'where'                   => 1,
                    'select'                  => 1,
                ],
                'result'               => [2],
                'partialReferences'    => [$entityTwo],
            ]],
            [[
                'options'              => [ 'multiple' => false, 'partialReference' => true ],
                'identifierFieldNames' => ['idOne', 'idTwo'],
                'typeOfField'          => [['idOne', 'integer'], ['idTwo', 'string']],
                'input'                => json_encode(['idOne' => 2, 'idTwo' => 'two']),
                'output'               => $entityTwo,
                'expects'              => [
                    'getManagerForClass'      => 4,
                    'getClassMetadata'        => 3,
                    'getRepository'           => 1,
                    'getIdentifierFieldNames' => 1,
                    'getTypeOfField'          => 2,
                    'createQueryBuilder'      => 1,
                    'getQuery'                => 1,
                    'getResult'               => 1,
                    'expr'                    => 2,
                    'setParameter'            => 2,
                    'where'                   => 1,
                    'select'                  => 1,
                ],
                'result'               => [2],
                'partialReferences'    => [$entityTwo],
            ]],
            [[
                'options'              => [ 'multiple' => true, 'partialReference' => true ],
                'identifierFieldNames' => ['idOne'],
                'typeOfField'          => [['idOne', 'integer']],
                'input'                => json_encode([1, 2]),
                'output'               => [new Entity(1), new Entity(2)],
                'expects'              => [
                    'getManagerForClass'      => 4,
                    'getClassMetadata'        => 3,
                    'getRepository'           => 1,
                    'getIdentifierFieldNames' => 1,
                    'getTypeOfField'          => 2,
                    'createQueryBuilder'      => 1,
                    'getQuery'                => 1,
                    'getResult'               => 1,
                    'expr'                    => 3,
                    'setParameter'            => 2,
                    'where'                   => 1,
                    'select'                  => 1,
                ],
                'result'               => [1, 2],
                'partialReferences'    => [new Entity(1), new Entity(2)],
            ]],
            [[
                'options'              => [ 'multiple' => true, 'partialReference' => false ],
                'identifierFieldNames' => ['idOne'],
                'typeOfField'          => [['idOne', 'integer']],
                'input'                => json_encode([1, 2]),
                'output'               => [new Entity(1), new Entity(2)],
                'expects'              => [
                    'getManagerForClass'      => 4,
                    'getClassMetadata'        => 3,
                    'getRepository'           => 1,
                    'getIdentifierFieldNames' => 1,
                    'getTypeOfField'          => 2,
                    'createQueryBuilder'      => 1,
                    'getQuery'                => 1,
                    'getResult'               => 1,
                    'expr'                    => 3,
                    'setParameter'            => 2,
                    'where'                   => 1,
                    'select'                  => 0,
                ],
                'result'               => [new Entity(1), new Entity(2)],
            ]],
        ];
    }

    /**
     * @dataProvider providerReverseTransform
     *
     * @param $data
     */
    public function testReverseTransform($data)
    {

        $this->registry
            ->expects($this->exactly($data['expects']['getManagerForClass'] ?? 0))
            ->method('getManagerForClass')
            ->willReturn($this->entityManager);

        $this->entityManager
            ->expects($this->exactly($data['expects']['getClassMetadata'] ?? 0))
            ->method('getClassMetadata')
            ->willReturn($this->classMetadata);

        $this->classMetadata
            ->expects($this->exactly($data['expects']['getIdentifierFieldNames'] ?? 0))
            ->method('getIdentifierFieldNames')
            ->willReturn($data['identifierFieldNames']);

        $this->classMetadata
            ->expects($this->exactly($data['expects']['getTypeOfField'] ?? 0))
            ->method('getTypeOfField')
            ->willReturnMap($data['typeOfField'] ?? []);
            //->willReturn(null);

        $this->entityManager
            ->expects($this->exactly($data['expects']['getRepository'] ?? 0))
            ->method('getRepository')
            ->willReturn($this->entityRepository);

        $this->entityRepository
            ->expects($this->exactly($data['expects']['createQueryBuilder'] ?? 0))
            ->method('createQueryBuilder')
            ->willReturn($this->queryBuilder);

        // Reason: identifier count
        $expr = new Query\Expr();
        $this->queryBuilder
            ->expects($this->exactly($data['expects']['expr']))
            ->method('expr')
            ->willReturn($expr);

        // Reason: identifier count
        $this->queryBuilder
            ->expects($this->exactly($data['expects']['setParameter']))
            ->method('setParameter');

        $this->queryBuilder
            ->expects($this->exactly($data['expects']['where']))
            ->method('where');

        // Reason: partialReference
        $this->queryBuilder
            ->expects($this->exactly($data['expects']['select']))
            ->method('select');

        // Reason: partialReference
        $this->queryBuilder
            ->expects($this->exactly($data['expects']['getQuery'] ?? 0))
            ->method('getQuery')
            ->willReturn($this->query);

        $this->query
            ->expects($this->exactly($data['expects']['getResult'] ?? 0))
            ->method('getResult')
            ->willReturn($data['result']);

        if ($data['options']['partialReference'] ?? false) {
            $this->query
                ->expects($this->exactly(1))
                ->method('setHint');
            $this->entityManager
                ->expects($this->exactly(count($data['partialReferences'])))
                ->method('getPartialReference')
                ->will(new \PHPUnit_Framework_MockObject_Stub_ConsecutiveCalls($data['partialReferences']))
            ;
        }

        $transformer = new EntityToIdTransformer(Entity::class, new AutoCodec(new JsonCodec()), $data['options']);
        $transformer->setManagerRegistry($this->registry);
        $this->assertEquals($data['output'], $transformer->reverseTransform($data['input']));
    }
}

class Entity
{
    private $idOne;

    private $idTwo;

    /**
     * Entity constructor.
     *
     * @param mixed $idOne
     * @param mixed $idTwo
     */
    public function __construct($idOne = null, $idTwo = null)
    {
        $this->setIdOne($idOne);
        $this->setIdTwo($idTwo);
    }

    /**
     * @return mixed
     */
    public function getIdOne()
    {
        return $this->idOne;
    }

    /**
     * @param mixed $idOne
     */
    public function setIdOne($idOne)
    {
        $this->idOne = $idOne;
    }

    /**
     * @return mixed
     */
    public function getIdTwo()
    {
        return $this->idTwo;
    }

    /**
     * @param mixed $idTwo
     */
    public function setIdTwo($idTwo)
    {
        $this->idTwo = $idTwo;
    }
}
